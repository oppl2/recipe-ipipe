from conans import ConanFile, AutoToolsBuildEnvironment, tools
from conans.errors import ConanInvalidConfiguration
from conans.tools import os_info, SystemPackageTool
import contextlib
import os
required_conan_version = ">=1.33.0"

class PipePatchConan(ConanFile):
    name = "pipepatch"
    license = "GPL-2.0"
    description = "linux kernel PipePatch"
    topics = ("linux kernel PipePatch")
    settings = "arch"
    _arch = None
    exports_sources="*"

    def deploy(self):
        self.copy("ipipe-core-%s-%s-8.patch" % (self.version,self._arch),src="",dst="")

    def package(self):
        if self.settings.arch == "x86_64":
            self._arch="x86"
        print("ipipe-core-%s-%s-8.patch" % (self.version,self._arch))
        self.copy("ipipe-core-%s-%s-8.patch" % (self.version,self._arch),src="",dst="")
        